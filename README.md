# A sample Gradle template

This project is a template in Gradle using jFrog's [Gradle Artifactory Plugin].

The template describes a Python example, using a *src/* directory for containing 
python artifacts.

The project will publish json build information and deploy a zip file to 
an Artifactory Gradle repository. A `gradle.properties` file included in the project
has gradle configuration and project variable values for variables contained in 
the `build.gradle` config.

The `build.gradle` configuration is a compilation of Gradle script snippets and 
information taken from the following pages: 

- [Gradle Artifactory Plugin]
- [Gradle Artifactory Plugin Examples]
- [Gradle User Manual: Getting Started]
- [JFrog Knowledge Base: How to deploy/resolve different artifact types]
- [JFrog Bintray build-info-extractor-gradle plugin]


---

### `task pythonPackageDistribution`

A task called `pythonPackageDistribution` creates the zip file artifact to be deployed
to Artifactory. The Zip object properties and behaviors are imported from the 
Gradle [Base Plugin]. The zip file name's properties are hard-coded in the task. 
By default, the created zip file is located in the *build/distribution/* directory. 
The project configures the zip file to be an element to be published to Artifactory.


[Gradle Artifactory Plugin]: https://www.jfrog.com/confluence/display/JFROG/Gradle+Artifactory+Plugin
[Gradle Artifactory Plugin Examples]: https://github.com/JFrog/project-examples/tree/master/gradle-examples
[Gradle User Manual: Getting Started]: https://docs.gradle.org/current/userguide/getting_started.html
[JFrog Knowledge Base: How to deploy/resolve different artifact types]: https://jfrog.com/knowledge-base/how-to-deploy-resolve-different-artifact-types-through-a-gradle-job/
[JFrog Bintray build-info-extractor-gradle plugin]: https://bintray.com/jfrog/jfrog-jars/build-info-extractor-gradle
[Base Plugin]: https://docs.gradle.org/current/userguide/base_plugin.html#base_plugin